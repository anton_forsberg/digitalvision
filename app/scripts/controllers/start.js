'use strict';

/**
 * @ngdoc function
 * @name digitalVisionApp.controller:StartCtrl
 * @description
 * # StartCtrl
 * Controller of the digitalVisionApp
 */
angular.module('digitalVisionApp')
  .controller('StartCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
