'use strict';

/**
 * @ngdoc function
 * @name digitalVisionApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the digitalVisionApp. Receives data from the leap service and passes it forward to the slider
 */
angular.module('digitalVisionApp')
  .controller('MainCtrl', function ($scope, TestService, $rootScope) {
    $scope.config=TestService.setup(293,165,100);
    $scope.configShowing=false
    $scope.configValues={width:0, height:0, distance:0};

    TestService.start();
    $rootScope.isPointing=false;
    $scope.direction=[];
    $scope.something = "WHAT";
    //On gesture, boradcast it downwards
    $scope.$on("gesture", function(info, gesture){
      console.log(gesture);
      $scope.something=gesture.type + " " + gesture.directionString;
      $rootScope.isPointing=false;
      $scope.$broadcast("swipe", gesture);
    });
    //On gesture, broadcast downwards in the scope
    $scope.$on("pointing", function(info, indexFinger){
      if(!$rootScope.isPointing){
        console.log(indexFinger);
      }
      for(var i=0;i<indexFinger.direction.length;i++){
        $scope.direction[i]=indexFinger.direction[i]*90;
      }
      $rootScope.pointingPx = indexFinger.pixelValue;
      $rootScope.isPointing=true;
      $scope.$broadcast("point", indexFinger);
    });
    $scope.showConfig=function(){
      $scope.configShowing=true;
    }
    $scope.applyConfig=function(){
      $scope.config=TestService.setup($scope.configValues.width,$scope.configValues.width,$scope.configValues.width);
      $scope.configShowing=false;
    }
    $scope.cancelConfig=function(){

      $scope.configShowing=false;
    }



  });
