'use strict';

/**
 * @ngdoc function
 * @name digitalVisionApp.controller:SlideCtrl
 * @description
 * # SlideCtrl
 * Controller of the digitalVisionApp. Controls the slider.
 */
angular.module('digitalVisionApp')
  .controller('SlideCtrl', function ($scope, $rootScope, $timeout) {
    $scope.currentSlide=0;
    $scope.slidesPerPage =3;
    var pointingTo = 0;
    var pointingCounter=0;
    $scope.chosenPerson = undefined;
    $scope.chosenTicket=undefined;
    $scope.chosenPayment=undefined;
    $rootScope.pointerStyle={};
    $scope.selectedSlide = null;
    var step=0;
    $scope.finished=false;
    var dummy = "Umami master cleanse bitters, crucifix sustainable everyday carry 8-bit asymmetrical authentic kale chips. Kickstarter tofu lo-fi sustainable, tacos deep v forage meh intelligentsia banjo. Offal mumblecore ramps hashtag. Gluten-free wolf meh venmo narwhal forage VHS, distillery authentic helvetica master cleanse affogato gentrify retro. Normcore bicycle rights irony, quinoa everyday carry shoreditch hoodie polaroid sartorial pour-over gentrify migas artisan fashion axe. Fanny pack pug ugh, mlkshk scenester four loko venmo hella ethical church-key ramps.";

    $scope.slideWidth = {width: window.innerWidth /$scope.slidesPerPage - 25 +"px"}
    $scope.sliderPos = 0;
    //Move slider left
    function slideLeft(){
      if($scope.currentSlide + $scope.slidesPerPage < $scope.currentChoices.length){
        $scope.sliderPos -=window.innerWidth/$scope.slidesPerPage;
        $scope.currentSlide++;
      }
    }
    //Move slider right
    function slideRight(){
      if($scope.currentSlide > 0){
        $scope.sliderPos +=window.innerWidth/$scope.slidesPerPage;
        $scope.currentSlide--;

      }
    }
    //On swipe action, swipe either left or right
    $scope.$on("swipe", function(info, swipe){
      switch(swipe.directionString){
        case "left":
        console.log("LEFT")
        slideLeft();
        break;
        case "right":
        slideRight();
        break;
        default:
        console.log("Swipe where?")
        break;
      }
    });
    //On point action, generate the red dot or update it.
    $scope.$on("point", function(info,finger){
      var indexer = window.innerWidth/$scope.slidesPerPage;

      for(var i=0;i<$scope.slidesPerPage;i++){
        if(Math.floor(finger.pixelValue/indexer)==i){
          //i is the one you're pointing at, s
          if(i==pointingTo){
            pointingCounter++;
          } else{
            pointingCounter=0;
            pointingTo=i;
          }
        }
      }

      updatePointerStyle(finger.pixelValue);
      if(pointingCounter>=15){
        pointingCounter=0;
        console.log("TEST");
        selectSlide($scope.currentSlide+pointingTo);
      }


    })
    //Updates pointer style
    function updatePointerStyle(pixel){
      var size = 50*(pointingCounter/10+1);
      $rootScope.pointerStyle.width=size+'px';
      $rootScope.pointerStyle.height=size+'px';
      $rootScope.pointerStyle.marginLeft=-size/2+'px';
      $rootScope.pointerStyle.left=pixel+'px';
      $rootScope.pointerStyle.top ="calc(50% - " +size/2+'px)';

    }
    //Selects a certain slide that the user has been pointing to
    function selectSlide(index){
      if(!$scope.chosenPerson){
        step =0;
        console.log("person");
        console.log($scope.currentChoices[index]);
        $scope.chosenPerson = angular.copy($scope.currentChoices[index]);
        console.log($scope.chosenPerson);
      } else if(!$scope.chosenTicket){
        console.log("ticket");
        step=1;
        $scope.chosenTicket = angular.copy($scope.currentChoices[index]);
        console.log($scope.chosenTicket);
      } else{
        step=2;
        $scope.chosenPayment = angular.copy($scope.currentChoices[index]);
      }

      $scope.selectedSlide=index;
      $rootScope.noPointer=true;
      $timeout(loadNewSlides,500);

    }
    //LOADS new slides to the view
    function loadNewSlides(){
      pointingCounter=0;
      $scope.selectedSlide=null;
      $rootScope.noPointer=false;
      if(step==0){
        $scope.currentChoices=ticketList;
      } else if(step ==1){
        $scope.currentChoices=paymentList;
      } else if(step==2){
        $scope.currentChoices=[];
        $scope.finished=true;
        $timeout(resetView, 5000);
      }
      $scope.sliderPos=0;
      $scope.currentSlide=0;
    }

    //Resets the view
    function resetView(){
      $scope.currentSlide=0;
      $scope.slidesPerPage =3;
      pointingTo = 0;
      pointingCounter=0;
      $scope.chosenPerson = undefined;
      $scope.chosenTicket=undefined;
      $scope.chosenPayment=undefined;
      $rootScope.pointerStyle={};
      $scope.selectedSlide = null;
      step=0;
      $scope.finished=false;
      $scope.currentChoices=$scope.topChoices;
      $scope.finished=false;

    }

    var paymentList=[
      {
        name:"Cash",
        icon:"fa-money",
        text :"Please feed the bills into the slot to pay for your: ",
        description:dummy
      },
      {
        name:"Card",
        icon:"fa-credit-card",
        text: "please follow the instructions to pay for your: ",
        description:dummy
      },
      {
        name:"Invoice",
        icon:"fa-file-text-o",
        text: "please scan your id to have an invoice sent to your home address. You're buying :",
        description:dummy
      }
    ];
    var ticketList = [
      {
        name:"Single Journey",
        price:100,
        icon:"fa-ticket",
        description:dummy
      },
      {
        name:"Return Trip",
        price:160,
        icon:"fa-ticket",
        description:dummy
      },
      {
        name:"Three Day Subscription",
        price:350,
        icon:"fa-credit-card",
        description:dummy
      },
      {
        name:"One Week Subscription",
        price:600,
        icon:"fa-credit-card",
        description:dummy
      },
      {
        name:"One Month Subscription",
        price:2100,
        icon:"fa-credit-card",
        description:dummy
      },
      {
        name:"One Year Subscription",
        price:22000,
        icon:"fa-credit-card",
        description:dummy
      }
    ];

    $scope.topChoices=[
      {
        name:"Childrens Tickets",
        discount:0.5,
        icon:"fa-child",
        description:dummy
      },
      {
        name:"Pensioners Tickets",
        icon:"fa-blind",
        discount:0.6,
        description:dummy
      },
      {
        name:"Regular Tickets",
        icon:"fa-user",
        discount:1,
        description:dummy
      },
      {
        name:"Student Tickets",
        icon:"fa-graduation-cap",
        discount:0.7,
        description:dummy
      },
      {
        name:"Group Tickets",
        icon:"fa-users",
        discount:0.5,
        minimum:10,
        description:dummy
      }
    ];


    $scope.currentChoices=$scope.topChoices;
  });
