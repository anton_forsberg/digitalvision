'use strict';

/**
 * @ngdoc function
 * @name digitalVisionApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the digitalVisionApp
 */
angular.module('digitalVisionApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
