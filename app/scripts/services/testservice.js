'use strict';

/**
 * @ngdoc service
 * @name digitalVisionApp.TestService
 * @description
 * # TestService
 * Service in the digitalVisionApp. In charge of interpreting the data from the leap motion.
 */
angular.module('digitalVisionApp')
  .service('TestService', function ($q, $rootScope) {
    var lastSent = new Date(86400000); //Beginning of time
    var interval = 400;
    var pointInterval =100;
    var controllerOptions = {enableGestures: true};
    var lastPoint;

    var screenW=0;
    var screenH=0;
    var leapDist=0;
    var maxLeftAngle=0;
    var maxRightAngle=0;
    var maxUpAngle=0;
    var maxDownAngle=0;
    var pxPerMm;

    //Configure the leap. Takes in width, height and distance to the leap in mm.
    this.setup=function(width, height, distance){
      screenW=width;
      screenH=height;
      leapDist=distance;

      var a=screenW/2;
      var b= Math.sqrt(a*a+leapDist*leapDist);
      maxLeftAngle=Math.abs(Math.asin(a*1/b))*-1; //radians
      maxLeftAngle=maxLeftAngle/(2*Math.PI)*360;
      maxRightAngle=-1*maxLeftAngle;
      var config={};
      config.maxLeft=maxLeftAngle;
      config.maxRight=maxRightAngle;

        pxPerMm= window.innerWidth/screenW;
      return config;

    }
    //Starts the leap
    this.start = function(){
      console.log("starting");
      Leap.loop(controllerOptions, function(frame) {
        getGestures(frame).then(function(data){
          var timeNow = new Date();
          if(timeNow-lastSent>interval){
            $rootScope.$broadcast("gesture",data);
            lastSent = new Date();
          } else{
            console.log("Not sending");
          }

        })
      });
    }

    //Gets gestures from a frame
    function getGestures(frame){
      var deferred = $q.defer();
      var currentGesture;
      for (var i = 0; i < frame.gestures.length; i++) {
        var gesture = frame.gestures[i];

        switch(gesture.type){
          case "swipe":
            currentGesture=gesture;
            currentGesture.directionString=getSwipeDirection(currentGesture);
            break;
          case "screenTap":
            currentGesture = gesture;
            break;
          default:
            currentGesture=null;
            break;
        }
      }
      //If no swipes, check if pointing
      if(frame.gestures.length ==0){

          for(var i=0; i<frame.hands.length;i++){
            checkPointing(frame.hands[i]);
          }
      }

      if(currentGesture==null){
        deferred.reject(currentGesture);
      } else{
        deferred.resolve(currentGesture);
      }
      return deferred.promise;

    }
    //Checks if a user is pointing
    function checkPointing(hand){
      var extended = 0;
      for(var i=0;i<hand.fingers.length;i++){
        if(hand.fingers[i].extended == true){
          extended++;
        }
      }
      //if pointing with index
      if(extended == 1 && hand.indexFinger.extended==true){
        hand.indexFinger.pixelValue=getPixelValue(hand.indexFinger.direction[0]);
        var timeNow = new Date();
        if(timeNow-lastSent>pointInterval){
          $rootScope.$broadcast("pointing",hand.indexFinger);
          lastSent = new Date();
        } else{
        }

      }

    }
    //Retrieves the pixel value the user is pointing to
    function getPixelValue(angle){

      //TODO Take hand position (distance from leap) into account

      //convert to radians
      angle=angle*Math.PI;

      var vc=Math.PI/2-Math.abs(angle);

      var distMM = Math.sin(angle)*leapDist/Math.sin(vc);
      var distPX=distMM*pxPerMm;
      var pixelValue= window.innerWidth/2 + distPX;
      lastPoint=pixelValue;
      return pixelValue;
    }
    //Determines swipe direction (Left/right)
    function getSwipeDirection(gesture){
      var isHorizontal = Math.abs(gesture.direction[0]) > Math.abs(gesture.direction[1]);
          //Classify as right-left or up-down
          if(isHorizontal){
              if(gesture.direction[0] > 0){
                  return "right";
              } else {
                  return "left";
              }
          } else { //vertical
              if(gesture.direction[1] > 0){
                  return "up";
              } else {
                  return "down";
              }
          }

    }
  });
