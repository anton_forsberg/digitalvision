'use strict';

describe('Service: LeapFactory', function () {

  // load the service's module
  beforeEach(module('digitalVisionApp'));

  // instantiate service
  var LeapFactory;
  beforeEach(inject(function (_LeapFactory_) {
    LeapFactory = _LeapFactory_;
  }));

  it('should do something', function () {
    expect(!!LeapFactory).toBe(true);
  });

});
