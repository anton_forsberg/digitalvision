'use strict';

describe('Service: LeapService', function () {

  // load the service's module
  beforeEach(module('digitalVisionApp'));

  // instantiate service
  var LeapService;
  beforeEach(inject(function (_LeapService_) {
    LeapService = _LeapService_;
  }));

  it('should do something', function () {
    expect(!!LeapService).toBe(true);
  });

});
